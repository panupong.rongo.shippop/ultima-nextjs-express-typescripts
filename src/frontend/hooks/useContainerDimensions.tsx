import { useEffect, useState, MutableRefObject } from 'react'
import ScreenDimention from '../../interfaces/ScreenDimention'

const useContainerDimensions = (
  myReference: MutableRefObject<null>,
): ScreenDimention => {
  const [dimensions, setDimensions] = useState<ScreenDimention>({
    width: 0,
    height: 0,
  })

  useEffect(() => {
    const getDimensions = (): ScreenDimention => {
      if (myReference && myReference.current) {
        const {
          offsetWidth,
          offsetHeight,
        }: { offsetWidth: number; offsetHeight: number } = myReference.current

        return { width: offsetWidth, height: offsetHeight }
      }
      return { width: 0, height: 0 }
    }
    const handleResize = () => {
      setDimensions(getDimensions())
    }

    if (myReference && myReference.current) {
      setDimensions(getDimensions())
    }

    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [myReference])

  return dimensions
}
export default useContainerDimensions
